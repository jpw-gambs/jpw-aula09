### Exercicio 39

> Implemente uma API REST capaz de responder as solicitações de um CRUD para
> o conjunto de dados (link). Utilize um arquivo JSON para armazenar e manipular
> os dados da API. Crie rotas para cada um dos resources encontrados no arquivo
> JSON.
