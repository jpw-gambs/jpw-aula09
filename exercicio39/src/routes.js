const express = require('express')
const router = express.Router();
const UsuarioController = require('./controllers/UsuarioController');
const LocalController = require('./controllers/LocalController');
const OficinaController = require('./controllers/OficinaController');

router.get("/usuarios", UsuarioController.index);
router.post("/usuarios", UsuarioController.store);
router.put("/usuarios/:id", UsuarioController.update);
router.delete("/usuarios/:id", UsuarioController.destroy);

router.get("/locais", LocalController.index);
router.post("/locais", LocalController.store);
router.put("/locais/:id", LocalController.update);
router.delete("/locais/:id", LocalController.destroy);

router.get("/oficinas", OficinaController.index);
router.post("/oficinas", OficinaController.store);
router.put("/oficinas/:id", OficinaController.update);
router.delete("/oficinas/:id", OficinaController.destroy);

module.exports = router;