const fs = require('fs');

module.exports = {
    async index(req, res) {
        const data = fs.readFileSync(__dirname + "/../data/locais.json", "utf8");
        const locais = JSON.parse(data);
        res.send(locais);
    },
    async store(req, res) {
        const { nome, capacidade } = req.body;

        const data = fs.readFileSync(__dirname + "/../data/locais.json", "utf8");
        const locais = JSON.parse(data);

        const local = { _id: locais.length + 1, nome, capacidade };

        locais.push(local);

        fs.writeFileSync(__dirname + "/../data/locais.json", JSON.stringify(locais), { encoding: 'utf8', flag: 'w' });

        res.send(local);
    },
    async update(req, res) {
        const id = req.params.id;
        const { nome, capacidade } = req.body;
        const data = fs.readFileSync(__dirname + "/../data/locais.json", "utf8");
        const locais = JSON.parse(data);

        let local = {};

        locais.forEach(element => {
            if (element._id == id) {
                element.nome = nome;
                element.capacidade = capacidade;
                local = element;
            }
        });

        fs.writeFileSync(__dirname + "/../data/locais.json", JSON.stringify(locais), { encoding: 'utf8', flag: 'w' });

        res.send(local)
    },
    async destroy(req, res) {
        const id = req.params.id;
        const data = fs.readFileSync(__dirname + "/../data/locais.json", "utf8");
        const locais = JSON.parse(data);

        var deleted = locais.splice((id - 1), 1);

        fs.writeFileSync(__dirname + "/../data/locais.json", JSON.stringify(locais), { encoding: 'utf8', flag: 'w' });

        res.send(deleted)

    }
}