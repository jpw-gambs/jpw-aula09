const fs = require('fs');

module.exports = {
    async store(req, res) {
        const { oficina, participante } = req.body;

        const oficinaJson = fs.readFileSync(__dirname + "/../data/oficinas.json", "utf8");
        const locaisJson = fs.readFileSync(__dirname + "/../data/locais.json", "utf8");
        const oficinas = JSON.parse(oficinaJson);
        const locais = JSON.parse(locaisJson);
        let msg = '';

        oficinas.forEach(element => {
            if (element._id == oficina) {
                msg = 'Oficina encontrada';
                locais.forEach(e => {
                    if (e._id == element.local) {
                        if (element.participantes.length >= e.capacidade) {
                            msg = 'A Capacidade máxima foi atingida';
                        } else {
                            msg += ', participante ' + participante + ' adicionado.';
                            element.participantes.push(participante);
                        }
                    }
                });
            }
        });

        fs.writeFileSync(__dirname + "/../data/oficinas.json", JSON.stringify(oficinas), { encoding: 'utf8', flag: 'w' });

        res.send(msg);
    },
    async destroy(req, res) {
        const { oficina, participante } = req.body;
        const data = fs.readFileSync(__dirname + "/../data/oficinas.json", "utf8");
        const oficinas = JSON.parse(data);

        oficinas.forEach(element => {
            if (element._id == oficina) {
                console.log(element.participantes);
                if (element.participantes.includes(participante)) {
                    element.participantes.pop(participante);
                }
            }
        })

        fs.writeFileSync(__dirname + "/../data/oficinas.json", JSON.stringify(oficinas), { encoding: 'utf8', flag: 'w' });

        res.send("Participante " + participante + " removido da oficina " + oficina + ".")
    }
}