const fs = require('fs');

module.exports = {
    async index(req, res) {
        const data = fs.readFileSync(__dirname + "/../data/oficinas.json", "utf8");
        const oficinas = JSON.parse(data);
        res.send(oficinas);
    },
    async store(req, res) {
        const { nome, professor, local, participantes } = req.body;

        const data = fs.readFileSync(__dirname + "/../data/oficinas.json", "utf8");
        const oficinas = JSON.parse(data);

        const oficina = { _id: oficinas.length + 1, nome, professor, local, participantes };

        oficinas.push(oficina);

        fs.writeFileSync(__dirname + "/../data/oficinas.json", JSON.stringify(oficinas), { encoding: 'utf8', flag: 'w' });

        res.send(oficina);
    },
    async update(req, res) {
        const id = req.params.id;
        const { nome, professor, local, participantes } = req.body;
        const data = fs.readFileSync(__dirname + "/../data/oficinas.json", "utf8");
        const oficinas = JSON.parse(data);

        const oficina = { _id: id, nome, professor, local, participantes };

        oficinas.forEach(element => {
            if (element._id == id) {
                element.nome = nome;
                element.professor = professor;
                element.local = local;
                element.participantes = participantes;
            }
        });

        fs.writeFileSync(__dirname + "/../data/oficinas.json", JSON.stringify(oficinas), { encoding: 'utf8', flag: 'w' });

        res.send(oficinas[id - 1])
    },
    async destroy(req, res) {
        const id = req.params.id;
        const data = fs.readFileSync(__dirname + "/../data/oficinas.json", "utf8");
        const oficinas = JSON.parse(data);

        var deleted = oficinas.splice((id - 1), 1);

        fs.writeFileSync(__dirname + "/../data/oficinas.json", JSON.stringify(oficinas), { encoding: 'utf8', flag: 'w' });

        res.send(deleted)

    }
}