const fs = require('fs');

module.exports = {
    async index(req, res) {
        const data = fs.readFileSync(__dirname + "/../data/usuarios.json", "utf8");
        const usuarios = JSON.parse(data);
        res.send(usuarios);
    },
    async store(req, res) {
        const { nome } = req.body;

        const data = fs.readFileSync(__dirname + "/../data/usuarios.json", "utf8");
        const usuarios = JSON.parse(data);

        const usuario = { _id: usuarios.length + 1, nome };

        usuarios.push(usuario);

        fs.writeFileSync(__dirname + "/../data/usuarios.json", JSON.stringify(usuarios), { encoding: 'utf8', flag: 'w' });

        res.send(usuario);
    },
    async update(req, res) {
        const id = req.params.id;
        const { nome } = req.body;
        const data = fs.readFileSync(__dirname + "/../data/usuarios.json", "utf8");
        const usuarios = JSON.parse(data);

        let user = {};

        usuarios.forEach(element => {
            if (element._id == id) {
                element.nome = nome;
                user = element;
            }
        });

        fs.writeFileSync(__dirname + "/../data/usuarios.json", JSON.stringify(usuarios), { encoding: 'utf8', flag: 'w' });

        res.send(user)
    },
    async destroy(req, res) {
        const id = req.params.id;
        const data = fs.readFileSync(__dirname + "/../data/usuarios.json", "utf8");
        const usuarios = JSON.parse(data);

        var deleted = usuarios.splice((id - 1), 1);

        fs.writeFileSync(__dirname + "/../data/usuarios.json", JSON.stringify(usuarios), { encoding: 'utf8', flag: 'w' });

        res.send(deleted)

    }
}