### Exercicio 40

> Implemente uma rota para o exercício anterior chamada check-in , que permita
> inserir e remover pessoas de uma determinada oficina . O número máximo de
> pessoas não deve ultrapassar a capacidade máxima do local .
> Caso ultrapasse, a api deve retornar uma mensagem de erro ao usuário, no modelo:
> {
> "erro": {
> "msg": "A capacidade máxima foi atingida",
> "codigo": 400
> }
> }
